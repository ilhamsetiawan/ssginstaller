# color
black='\033[1;30m'
red='\033[1;31m'
green='\033[1;32m'
yellow='\033[1;33m'
blue='\033[1;34m'
purple='\033[1;35m'
cyan='\033[1;36m'
white='\033[1;37m'



# cd home
cd /home/$USER
#

echo -e $red "        _//     _//_////////_//      _//    _////     "     
echo -e $red"       _//     _//_//       _//   _//    _//    _//  "     
echo -e $red"      _//     _//_//        _// _//   _//        _// "    
echo -e $red"    _////// _//_//////      _//     _//        _// "    
echo -e $red"   _//     _//_//        _// _//   _//        _//"     
echo -e $red"  _//     _//_//       _//   _//    _//     _// "    
echo -e $red" _//     _//_////////_//      _//    _////     "   






echo -e $red "welcome to hexo installer !!"


# give a name to your site
echo                "=================================="

echo -e  $green         + give a name to your site +
echo -e $blue site name :

# read name your site
read varname

#install hexo ssg
hexo init $varname


echo -e $red " __                              __     __                     __                "
echo -e $red " |  |--.---.-.-----.-----.--.--. |  |--.|  |.-----.-----.-----.|__|.-----.-----. "
echo -e $red " |     |  _  |  _  |  _  |  |  | |  _  ||  ||  _  |  _  |  _  ||  ||     |  _  | "
echo -e $red " |__|__|___._|   __|   __|___  | |_____||__||_____|___  |___  ||__||__|__|___  | "
echo -e $red "             |__|  |__|  |_____|                  |_____|_____|          |_____| "
#echo -e $white "happy blogging"

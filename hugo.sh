# color
black='\033[1;30m'
red='\033[1;31m'
green='\033[1;32m'
yellow='\033[1;33m'
blue='\033[1;34m'
purple='\033[1;35m'
cyan='\033[1;36m'
white='\033[1;37m'

# cd home
cd /home/$USER
echo -e $red "welcome to Hugo installer !!"


echo -e $red "         _    _ _    _  _____  ____  "
echo -e $red "        | |  | | |  | |/ ____|/ __ \ "
echo -e $red "        | |__| | |  | | |  __| |  | |"
echo -e $red "        |  __  | |  | | | |_ | |  | |"
echo -e $red "        | |  | | |__| | |__| | |__| |"
echo -e $red "        |_|  |_|\____/ \_____|\____/ "


echo -e $purple " >> download hugo "

#download
wget https://github.com/gohugoio/hugo/releases/download/v0.57.1/hugo_0.57.1_Linux-64bit.tar.gz
 
clear

echo -e $green " Extract your hugo installer"

#Extract
sudo tar xvzf hugo_0.57.1_Linux-64bit.tar.gz -C /usr/local/bin/

hugo version



# give a name to your site
echo                "================================="
echo -e  $green         + give a name to your site +
echo                "=================================="
echo -e $blue site name :

# read name your site
read varname

# create hexo site
hugo new site $varname

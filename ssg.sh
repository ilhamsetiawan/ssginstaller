black='\033[1;30m'
red='\033[1;31m'
green='\033[1;32m'
yellow='\033[1;33m'
blue='\033[1;34m'
purple='\033[1;35m'
cyan='\033[1;36m'
white='\033[1;37m'

clear


echo -e $red   "   ____ ____ ____    _ _  _ ____ ___ ____ _    _    ____ ____ "
echo -e $green "   [__  [__  | __    | |\ | [__   |  |__| |    |    |___ |__/ "
echo -e $blue  "   ___] ___] |__]    | | \| ___]  |  |  | |___ |___ |___ |  \ "
echo -e $purple" -------------------------------------------------V.1.0-------
"
                               
    
echo -e $purple " Created By Ilham Setiawan"
echo -e $yellow "Visit my blog : https://ilhamsetiawan.web.id"

echo -e $blue "https://gitlab.com/ilhamsetiawan
"

echo -e $green "+Select static site generator+
"



PS3='Please enter your choice: '
options=("Hexo" "Hugo" "Jeklyn" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Hexo")
            bash hexo.sh
            ;;
        "Hugo")
            bash hugo.sh
            ;;
        "Jeklyn")
            echo "you chose choice $REPLY which is $opt"
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY" ;;
    esac
done
